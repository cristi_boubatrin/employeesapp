package com.pentastagiu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Cristi
 *
 */
@Entity
@Table(name = "socialid")
public class Social_id {
	@Id
	@Column(name = "socialid")
	@GeneratedValue
	private Long social_id;
	@Column(name = "socialidtype")
	private String social_id_type;

	/**
	 * Constructor with parameters
	 * 
	 * @param social_id
	 * @param social_id_type
	 */
	public Social_id(Long social_id, String social_id_type) {
		this.social_id = social_id;
		this.social_id_type = social_id_type;
	}

	/**
	 * Null constructor
	 */
	public Social_id() {

	}

	public Long getSocial_id() {
		return social_id;
	}

	public void setSocial_id(Long social_id) {
		this.social_id = social_id;
	}

	public String getSocial_id_type() {
		return social_id_type;
	}

	public void setSocial_id_type(String social_id_type) {
		this.social_id_type = social_id_type;
	}

}
