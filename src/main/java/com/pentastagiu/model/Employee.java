package com.pentastagiu.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author Cristi Employee class
 *
 */
@Entity
@Table(name = "employee")
public class Employee {
	@Id
	@Column(name = "emp_id")
	@GeneratedValue
	private Long emp_id;
	@Column(name = "emp_name")
	private String emp_name;
	@ManyToOne
	private Department department;
	@OneToOne
	private Social_id social_id;
	@ManyToMany
	private Set<Project> projects;

	/**
	 * Constructor with parameters
	 * 
	 * @param emp_id
	 * @param emp_name
	 * @param department
	 * @param social_id
	 * @param projects
	 */
	public Employee(Long emp_id, String emp_name, Department department, Social_id social_id, Set<Project> projects) {
		super();
		this.emp_id = emp_id;
		this.emp_name = emp_name;
		this.department = department;
		this.social_id = social_id;
		this.projects = projects;
	}

	/**
	 * Null constructor
	 */
	public Employee() {

	}

	public Long getId() {
		return emp_id;
	}

	public void setId(Long emp_id) {
		this.emp_id = emp_id;
	}

	public String getName() {
		return emp_name;
	}

	public void setName(String emp_name) {
		this.emp_name = emp_name;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Social_id getSocial_id() {
		return social_id;
	}

	public void setSocial_id(Social_id social_id) {
		this.social_id = social_id;
	}

	public Set<Project> getProjects() {
		return projects;
	}

	public void setProjects(Set<Project> projects) {
		this.projects = projects;
	}
}
