package com.pentastagiu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Cristi Project class
 */
@Entity
@Table(name = "project")
public class Project {
	@Id
	@Column(name = "project_id")
	@GeneratedValue
	private Long project_id;
	@Column(name = "project_name")
	private String project_name;

	/**
	 * Constructor with parameters
	 * 
	 * @param project_id
	 * @param project_name
	 */
	public Project(Long project_id, String project_name) {
		this.project_id = project_id;
		this.project_name = project_name;
	}

	/**
	 * Null constructor
	 */
	public Project() {

	}

	public String getProject_name() {
		return project_name;
	}

	public void setProject_name(String project_name) {
		this.project_name = project_name;
	}

	public Long getProject_id() {
		return project_id;
	}

	public void setProject_id(Long project_id) {
		this.project_id = project_id;
	}
}
