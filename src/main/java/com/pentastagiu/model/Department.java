package com.pentastagiu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Cristi
 *
 */
@Entity
@Table(name = "department")
public class Department {
	@Id
	@Column(name = "dept_id")
	@GeneratedValue
	private Long dept_id;
	@Column(name = "dept_name")
	private String dept_name;
	@Column(name = "dept_description")
	private String dept_description;

	/**
	 * Null constructor
	 */
	public Department() {

	}

	/**
	 * Constructor with parameters
	 * 
	 * @param dept_id
	 * @param dept_name
	 * @param dept_description
	 */
	public Department(Long dept_id, String dept_name, String dept_description) {
		this.dept_id = dept_id;
		this.dept_name = dept_name;
		this.dept_description = dept_description;
	}

	public Long getDept_id() {
		return dept_id;
	}

	public void setDept_id(Long dept_id) {
		this.dept_id = dept_id;
	}

	public String getDept_name() {
		return dept_name;
	}

	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}

	public String getDept_description() {
		return dept_description;
	}

	public void setDept_description(String dept_description) {
		this.dept_description = dept_description;
	}

}
