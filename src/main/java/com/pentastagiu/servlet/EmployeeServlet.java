package com.pentastagiu.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pentastagiu.dao.EmployeeDAO;
import com.pentastagiu.dao.EmployeeDAOImpl;
import com.pentastagiu.model.Employee;

/**
 * Servlet implementation class Servlet
 */
public class EmployeeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EmployeeServlet() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException{
	EmployeeDAO employeeDao = new EmployeeDAOImpl();
	List<Employee> employeeList = new ArrayList<Employee>();

	if (request.getParameter("employee_id") != null) {
		Long employee_id = Long.parseLong(request.getParameter("employee_id"));
		Employee withId = employeeDao.findOneById(employee_id);
		employeeList.add(withId);
	} else {
		employeeList = employeeDao.listAll();
	}
	request.setAttribute("employee", employeeList);
	request.getRequestDispatcher("src/main/webapp/index.jsp").forward(request, response);


	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
