package com.pentastagiu.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.pentastagiu.model.Employee;

public class EmployeeDAOImpl implements EmployeeDAO {
	@PersistenceContext
	private EntityManager em;

	public void save(Employee emp) {
		em.getTransaction().begin();
		em.persist(emp);
		em.getTransaction().commit();

	}

	public void update(Employee emp) {
		em.getTransaction().begin();
		em.merge(emp);
		em.getTransaction().commit();

	}

	public void delete(Employee emp) {
		em.getTransaction().begin();
		em.remove(emp);
		em.getTransaction().commit();

	}

	public List<Employee> listAll() {
		em.getTransaction().begin();
		return em.createNamedQuery("Employee.getAll").getResultList();
	}

	public Employee findOneById(Long employee_id) {
		em.getTransaction().begin();
		return em.find(Employee.class, employee_id);

	}

	public Employee findOneByName(String employee_name) {
		em.getTransaction().begin();
		return em.find(Employee.class, employee_name);
	}

}
