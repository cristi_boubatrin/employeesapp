package com.pentastagiu.dao;

import java.util.List;

import com.pentastagiu.model.Employee;

public interface EmployeeDAO {
	
	public void save(Employee emp);

	public void update(Employee emp);

	public void delete(Employee emp);

	public List<Employee> listAll();

	public Employee findOneById(Long employee_id);

	public Employee findOneByName(String employee_name);
}
